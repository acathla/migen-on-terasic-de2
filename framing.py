#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 15 13:20:54 2018

@author: fabien
"""

from migen import *
from Mem import Mem
from migen.genlib.fifo import SyncFIFO
from migen.genlib.cdc import MultiReg

class comm(Module):
    def __init__(self, uart):
        START_BYTE = 0x12
        STOP_BYTE = 0x13
        ESC_BYTE = 0x7D
        BUF_MAX = 16
        self.error = Signal()
        self.submodules.serialbuf_rx = SyncFIFO(8,BUF_MAX)
        self.submodules.serialbuf_tx = SyncFIFO(8,BUF_MAX)
        self.rx_buf = Signal(8)
        self.rx_full = Signal()
        rx_ready_buf = Signal()
        rising_rx = Signal()
        buf_index = Signal(max=BUF_MAX)
        msg_end = Signal()  # Flag the end of a transmission of bytes
        self.count = Signal(16)

        self.submodules.rxbyte_fsm = FSM(reset_state="IDLE")

        self.sync += [
                rx_ready_buf.eq(uart.rx_ready),
                self.serialbuf_rx.din.eq(self.rx_buf),
                If(rising_rx,
                   uart.rx_ack.eq(1)
                ).Elif((~uart.rx_ready & rx_ready_buf),  # Falling edge of rx_ready
                    uart.rx_ack.eq(0))
                ]
        self.sync += [
                If((uart.rx_ready & ~rx_ready_buf), # Rising edge of rx_ready
                   rising_rx.eq(1),
                   self.rx_buf.eq(uart.rx_data)
                   )
                .Else(rising_rx.eq(0))
                 ]
   
        self.rxbyte_fsm.act("IDLE",
            If(rising_rx,   # Should not be up more than a clock cycle
               If(self.rx_buf == START_BYTE, # Wait for the start byte to arrive
                      NextValue(self.error, 0), # reset the error flag with a new start byte
                      NextState("START")
                )# Else, nothing happens
            )
        )
                     
        self.rxbyte_fsm.act("START",
            If(rising_rx,
               Case(self.rx_buf, {
                    START_BYTE: NextState("ERROR"),
                    STOP_BYTE: NextState("STOP"),
                    ESC_BYTE: NextState("ESC"),
                    "default": NextState("STORE")
               })
            )
        )
         
        self.rxbyte_fsm.act("STORE",
                  NextValue(self.serialbuf_rx.we, 1),
                  NextValue(self.count, self.count + 1),
                  NextState("DATA")
              )
        
        self.rxbyte_fsm.act("DATA",
            NextValue(self.serialbuf_rx.we, 0),
            NextValue(buf_index, buf_index + 1),
            NextState("START")
        )
        
        self.rxbyte_fsm.act("STOP",
           NextValue(msg_end, 1),
           NextValue(buf_index, 0),
           NextState("IDLE")
        )

        self.rxbyte_fsm.act("ESC",
            If(rising_rx,   # Wait for the next byte to arrive
               NextState("STORE")    # Now go store the data even if it's a flag, it's escaped
            )
        )

        self.rxbyte_fsm.act("ERROR",
            NextValue(self.error, 1),
            NextValue(buf_index, 0),
            NextState("IDLE")
        )
        
        


if __name__ == "__main__":
    import sys
    
    if sys.argv[1] == "sim":
        print("Testbenching framing module...")
        class UART():
            def __init__(self):
                self.rx_data = Signal(8)
                self.rx_ready = Signal()
                self.rx_ack = Signal()
                self.rx_error = Signal()
        
                self.tx_data = Signal(8)
                self.tx_ready = Signal()
                self.tx_ack = Signal()
        
        uart=UART()
        dut=comm(uart)
        START_BYTE = 0x12
        STOP_BYTE = 0x13
        ESC_BYTE = 0x7D
        def testbench(dut):
            for i in range(4):
                yield
            yield uart.rx_data.eq(0x42)
            for i in range(4):
                yield
            yield rx.eq(1)
            for i in range(4):
                yield
            yield rx.eq(0)
            for i in range(4):
                yield
            yield uart.rx_data.eq(START_BYTE)
            for i in range(4):
                yield
            yield rx.eq(1)
            for i in range(4):
                yield
            yield rx.eq(0)
            for i in range(4):
                yield
            yield uart.rx_data.eq(0x43)
            for i in range(4):
                yield
            yield rx.eq(1)
            for i in range(4):
                yield
            yield rx.eq(0)
            for i in range(4):
                yield
            yield uart.rx_data.eq(0x55)
            for i in range(4):
                yield
            yield rx.eq(1)
            for i in range(4):
                yield
            yield rx.eq(0)
            for i in range(4):
                yield
            yield uart.rx_data.eq(STOP_BYTE)
            for i in range(4):
                yield
            yield rx.eq(1)
            for i in range(4):
                yield
            yield rx.eq(0)
            for i in range(4):
                yield
            
            #while (yield uart.rx_ack) == 1: yield
            #assert (yield uart.rx_ack) == 0
            
                
                # TODO: Test more !
                    
        run_simulation(dut, testbench(dut), vcd_name="framing.vcd")