from migen import *
from Mem import Mem

class Lcd(Module):
    def __init__(self, lcd):
        # Signals to LCD module
        bl_on = Signal()
        on = Signal()
        data = Signal(8)
        self.refresh = Signal() # Order to refresh the text
        #self.done = Signal()    # Refresh done
        en = Signal()
        rs = Signal()
        rw = Signal()

        refresh = Signal(reset=1)
        self.trig_refresh = Signal()
        lut_size = 38
        cont = Signal(18)
        power = Signal()
        index = Signal(max=lut_size)
        Line1 = " Welcome to the "
        Line2 = "migendemo @ ISIR"
        SMALL_DELAY = 128
        ram_init = [0x38, 0x0C, 0x01, 0x06, 0x80] + [ord(i) for i in list(Line1)] + [0x0C0] + [ord(i) for i in list(Line2)] # Converts strings to integers
        self.lut = Mem(8, lut_size, ram_init)
        self.submodules += [ self.lut ]
        self.submodules.fsm = FSM(reset_state="START")
        
        ###
        
        if lcd != None:
            self.comb += [ lcd.bl_on.eq(1),
                lcd.on.eq(1),
                lcd.en.eq(en),
                lcd.rs.eq(rs),
                lcd.rw.eq(0),
                lcd.data.eq(data)
                ]
            BIG_DELAY=0x3FFFE
        else:
            BIG_DELAY=64


        #print("Delay = ",BIG_DELAY)
        self.comb += [
            self.lut.mem_rd_port.adr.eq(index)
            #refresh.eq(self.trig_refresh)
            ]

        self.sync += [
                If( self.trig_refresh,
                   refresh.eq(1)
                  ),
                If(cont == BIG_DELAY,
                    cont.eq(0),
                ).Else(
                    cont.eq(cont + 1))
                ,
                If( (refresh & ~self.fsm.ongoing("START")),    # Signal generator to write to the LCD module
                    If(cont == 2,
                        en.eq(1)
                    ),
                    If(cont == 4,   # 40ns + 40ns
                        data.eq(self.lut.mem_rd_port.dat_r)
                    ),
                    If(cont == 32,  # 40ns + 230ns (mini)
                        en.eq(0)
                    ),
                    If((index == 21) | (index < 5),
                        rs.eq(0)
                    ).Else(
                        rs.eq(1)
                        )
                )
            ]

        self.fsm.act("START",
                If(cont == BIG_DELAY,
                    NextState("INIT")
                    )
            )

        self.fsm.act("INIT",
                If(cont == BIG_DELAY,
                    NextValue(index, index + 1)
                ),
                If(index == 5,
                    NextState("DATA")
                    )
            )

        self.fsm.act("DATA",
                #self.lut.mem_wr_port.we.eq(0),
                If(cont == SMALL_DELAY,
                   NextValue(index, index + 1)
                ),
                If(index == lut_size ,
                   NextValue(index, 1),
                   NextValue(refresh, 0),
                   NextState("WAIT")
                   )
            )

        self.fsm.act("WAIT",
            If(refresh,
                   NextState("INIT")
            )
        )
        

if __name__ == "__main__":
    import sys
    if len(sys.argv) >1 :
        if sys.argv[1] == "sim":
            print("Testbenching LCD module...")
            dut=Lcd(None)
            def testbench(dut):
                for cycle in range(0x30*70):
                    yield
                yield dut.trig_refresh.eq(1)
                for cycle in range(42):
                    yield
                yield dut.trig_refresh.eq(0)
                for cycle in range(0x30*70):
                    yield
                
            run_simulation(dut, testbench(dut), vcd_name="lcd.vcd")
