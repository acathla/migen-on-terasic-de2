#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  5 14:38:37 2018

@author: fabien
"""

from migen import *
from migen.build.platforms import de2
from PWM import PWM
from quadrature import quadDec
from SevenSeg import SevenSeg
from UART import UART
from Lcd import Lcd
from vga import VGA
from Mem import Mem
from PLL import PLL
from framing import comm
from math import *

X=1920
Y=1080

class Top(Module):
    def __init__(self, vga, serial):
            self.pwm = PWM()
            self.decoder = quadDec()
            self.submodules += self.pwm, self.decoder, vga
            self.submodules.FB = Mem(11, X, [0])
            self.submodules.uart = UART(serial, clk_freq=50000000, baud_rate=9600)
            self.submodules.comm = comm(self.uart)
            
            tex_size = 64
            tex_init = []
            for i in range(tex_size):
                tex_init.append(2048+int(128*sin(i*2*pi/64)))
            self.submodules.texture = Mem(12, tex_size, tex_init)
            self.dir = Signal()
            pwr = Signal(bits_sign=(12,True))
            wr_adr = Signal(max=1920)
 
            
            ### Motor loop
            WALL = 509
            self.comb += [
                    pwr.eq(self.texture.mem_rd_port.dat_r - 2048),
                    If(pwr)
                    ]
            self.sync += [
                    ## Positive wall
                    If(self.decoder.position > WALL,
                       self.dir.eq(0),
                       self.pwm.pwm_in.eq((self.decoder.position - WALL)*16)
                    ## Negative wall
                    ).Elif(self.decoder.position < -WALL,
                       self.dir.eq(1),
                       self.pwm.pwm_in.eq((-self.decoder.position - WALL)*16)
                    ## Texture
                    ).Elif( (self.decoder.position < 192) & (self.decoder.position > -191),
                        self.texture.mem_rd_port.adr.eq(self.decoder.position),
                        If(pwr < 0,
                           self.dir.eq(1),
                           self.pwm.pwm_in.eq(-pwr),
                        ).Else(
                            self.dir.eq(0),
                            self.pwm.pwm_in.eq(pwr)
                        )
                    ).Else(
                            self.pwm.pwm_in.eq(0)
                            )
                    ]
                    
            
            ### VGA display of the position of the wheel
            self.comb += [
                    self.FB.mem_rd_port.adr.eq(vga.oCoord_X),
                    self.FB.mem_wr_port.we.eq(1)
                    ]
            self.sync += [
                    If(vga.blank == 1,
                        wr_adr.eq(wr_adr + 1),
                        self.FB.mem_wr_port.dat_w.eq(self.decoder.position + int(Y/2)),
                        self.FB.mem_wr_port.adr.eq(wr_adr)
                        )
                    ]
            self.sync.clk_pix += [
                    If(vga.oCoord_Y == self.FB.mem_rd_port.dat_r,
                       vga.red_in.eq(1023),
                       vga.blue_in.eq(0)
                       ).Else(
                           vga.red_in.eq(0),
                           vga.blue_in.eq(512) # nice blue background
                           )
                
                    ]
                
            self.clock_domains.cd_sys = ClockDomain("sys")
            
if __name__ == "__main__":
    plat = de2.Platform()
    clk50 = plat.request("clk50")
    pix_pll = PLL(20, "pix", 0, "NORMAL", 11, 4)
    serial = plat.request("serial")
    
    # VGA display of values
    #vga = VGA(plat.request("clk27"),plat.request("key",0))
    vga = VGA(pix_pll.clk_out,plat.request("key",0))
    vga_out = plat.request("vga_out")
    vga.comb += [ vga_out.g.eq(vga.green),
        vga_out.r.eq(vga.red),
        vga_out.b.eq(vga.blue),
        vga_out.hsync.eq(vga.hsync),
        vga_out.vsync.eq(vga.vsync),
        vga_out.blank.eq(~vga.blank),
        vga_out.sync.eq(~(vga.vsync & vga.hsync)),
        vga_out.clk.eq(vga.clk_in)
        ]
   
    
    top = Top(vga, serial)
    top.comb += top.cd_sys.clk.eq(clk50)
    top.comb += pix_pll.clk_in.eq(clk50)
            
    GPIO0 = plat.request("GPIO",0)
    GPIO1 = plat.request("GPIO",1)

    
    top.submodules.lcd = Lcd(plat.request("lcd"))
    top.submodules += pix_pll
    
    top.submodules.fsmcopy = FSM(reset_state="IDLE")
    
    top.comb += [   ## Motor control
            top.decoder.A.eq(GPIO0[1]),
            top.decoder.B.eq(GPIO0[0]),
            GPIO1[0].eq(top.pwm.PWM),
            plat.request("user_led").eq(top.pwm.PWM),
            GPIO1[2].eq(top.dir),
            GPIO1[3].eq(~top.dir),
            plat.request("user_led").eq(top.dir),
            plat.request("user_led").eq(~top.dir),
            plat.request("user_redled").eq(top.comm.error),                          # LED 0
            plat.request("user_redled").eq(top.fsmcopy.ongoing("IDLE")),
            plat.request("user_redled").eq(top.fsmcopy.ongoing("COPY")),    # LED 2
            plat.request("user_redled").eq(top.fsmcopy.ongoing("END")),
            plat.request("user_redled").eq(top.comm.rxbyte_fsm.ongoing("DATA")),     # LED 4
            plat.request("user_redled").eq(top.comm.rxbyte_fsm.ongoing("STOP")),
            plat.request("user_redled").eq(top.comm.rxbyte_fsm.ongoing("ESC")),      # LED 6
            plat.request("user_redled").eq(top.comm.serialbuf_rx.writable),
            plat.request("user_redled").eq(top.uart.rx_error),                       # LED 8
            plat.request("user_redled").eq(top.comm.serialbuf_rx.we),
            plat.request("user_redled").eq(top.comm.serialbuf_rx.re),                # LED 10
            plat.request("user_redled").eq(top.comm.serialbuf_rx.readable)

            ]
    
    addr = Signal(max=38, reset=5)
    bufread = Signal()
    
    
    top.comb += [top.lcd.lut.mem_wr_port.adr.eq(addr),
                 top.lcd.lut.mem_wr_port.dat_w.eq(top.comm.serialbuf_rx.dout)
                 ]
    
    top.fsmcopy.act("IDLE",
                    If(top.comm.serialbuf_rx.readable,
                       NextValue(top.lcd.lut.mem_wr_port.we, 1),
                       #NextValue(top.comm.serialbuf_rx.re, 1),
                       NextState("COPY")
                       )
                    )
    top.fsmcopy.act("COPY",
                       NextValue(top.lcd.lut.mem_wr_port.we, 0),
                       NextValue(top.comm.serialbuf_rx.re, 1),
                       If(addr==20,
                             NextValue(addr, addr + 2),
                          ).Elif(addr==37,
                                 NextValue(addr, 5)
                                 ).Else(
                                         NextValue(addr, addr+1)
                        ),
                        NextValue(top.lcd.trig_refresh,1),
                       NextState("END")
                    )
    top.fsmcopy.act("END",
                    NextValue(top.comm.serialbuf_rx.re, 0),
                    NextValue(top.lcd.trig_refresh,0),
                    If(True, #~top.comm.serialbuf_rx.readable,
                       NextState("IDLE")
                      )
                )
    top.comb += [
            plat.request("user_redled").eq(top.fsmcopy.ongoing("IDLE")),    # LED 12
            plat.request("user_redled").eq(top.fsmcopy.ongoing("COPY")),
            plat.request("user_redled").eq(top.fsmcopy.ongoing("END")),    # LED 14
            plat.request("user_redled").eq(top.uart.rx_fsm.ongoing("ERROR")),    # LED 17    
            ]
    seg=[SevenSeg() for n in range(8)]
    for i in range(8):
        # Plugs outputs of SevenSeg module to physical 7 segments module
        seg[i].comb += [plat.request("hex",i).SevenSeg.eq(seg[i].segments) ]         
        top.submodules += seg[i]
    top.comb += [ seg[0].value.eq(top.comm.serialbuf_rx.level[0:4]),
                 seg[1].value.eq(top.lcd.lut.mem_wr_port.dat_w[0:4]),
                 seg[2].value.eq(top.lcd.lut.mem_wr_port.dat_w[4:8]),
                 seg[3].value.eq(top.comm.serialbuf_rx.dout[0:4]),
                 seg[4].value.eq(0),
                 seg[5].value.eq(top.pwm.pwm_in[4:8]),
                 seg[6].value.eq(top.comm.rx_buf[0:4]),
                 seg[7].value.eq(top.comm.rx_buf[4:8])]    
                
    
    plat.build(top, run=True, build_dir="asserv", build_name="asserv", toolchain_path="/home/fabien/altera/13.0sp1/quartus/bin/")