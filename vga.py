#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  6 18:24:36 2018


  FRONT     SYNC    BACK                                FRONT
  PORCH    ______  PORCH    _   _   _        _   _   _  PORCH (again)
__________|      |_________| |_| |_| |PIXELS| |_| |_| |________|



@author: fabien

"""
from migen import *
from PLL import PLL

class VGA(Module):
    def __init__(self, clk_pix, rst):
        # Maximums you want, will set size of registers below
        # Defaults from 1920x1440@75Hz VESA timings http://tinyvga.com/vga-timing/1920x1440@75Hz
        #Modeline "1920x1080R"  138.50  1920 1968 2000 2080  1080 1083 1088 1111 +hsync -vsync
        MAX_X = 1920
        MAX_XLINE = MAX_X + 720    # MAX X resolution + timings
        MAX_Y = 1440
        MAX_YLINE = MAX_Y + 60
        MAX_REZ = MAX_X*MAX_Y    
        # H
        H_SYNC_ACT   = 1920
        H_SYNC_FRONT = 88
        H_SYNC_CYC   = 44
        H_SYNC_BACK  = 148
        X_START      = H_SYNC_FRONT + H_SYNC_CYC + H_SYNC_BACK
        H_SYNC_TOTAL = H_SYNC_ACT + X_START
        # V
        V_SYNC_FRONT = 4
        V_SYNC_CYC   = 5
        V_SYNC_BACK  = 36
        V_SYNC_ACT   = 1080
        Y_START      = V_SYNC_FRONT + V_SYNC_CYC + V_SYNC_BACK
        V_SYNC_TOTAL = V_SYNC_ACT + Y_START
        
        #	Horizontal registers
        h_sync_front  =	Signal(max=48, reset=H_SYNC_FRONT)
        h_sync_cyc	=	Signal(max=224, reset=H_SYNC_CYC)
        h_sync_back	=	Signal(max=352, reset=H_SYNC_BACK)
        h_sync_act	=	Signal(max=MAX_X, reset=H_SYNC_ACT)
        
        h_sync_total  =	Signal(max=MAX_XLINE, reset=H_SYNC_TOTAL)
        #	Vertical registers
        v_sync_front  =	Signal(max=8, reset=V_SYNC_FRONT)
        v_sync_cyc	=	Signal(max=8, reset=V_SYNC_CYC)
        v_sync_back	=	Signal(max=56, reset=V_SYNC_BACK)
        v_sync_act	=	Signal(max=MAX_Y, reset=V_SYNC_ACT)
        v_sync_total  =	Signal(max=MAX_YLINE, reset=V_SYNC_TOTAL)
        #	Start Offset
        x_start		=	Signal(max=720, reset=X_START)
        y_start		=	Signal(max=60, reset=Y_START)


        # Inputs
        self.clk_in = Signal()
        self.red_in = Signal(10)
        self.blue_in = Signal(10)
        self.green_in = Signal(10)

        # Outputs
        self.red = Signal(10)
        self.green = Signal(10)
        self.blue = Signal(10)
        self.hsync = Signal()
        self.vsync = Signal()
        self.clk_out = Signal()
        self.adr = Signal(max=MAX_REZ*3)

        # Internal
        H_Cont = Signal(max=MAX_X) # Will be 10 bits (512 < 800 < 1024)
        V_Cont = Signal(max=MAX_Y) # idem
        self.blank = Signal()
        self.oCoord_X = Signal(max=MAX_X)
        self.oCoord_Y = Signal(max=MAX_Y)
        self.oAddress = Signal(max=MAX_REZ)


        # Clocks
        self.clock_domains.cd_clk_pix = ClockDomain()

        ###

        self.comb += [ self.cd_clk_pix.clk.eq(clk_pix),
                self.cd_clk_pix.rst.eq(~rst)
                ]
        self.comb += self.clk_in.eq(ClockSignal("clk_pix"))
       
        self.comb += self.clk_out.eq(ClockSignal("clk_pix"))
    


        self.sync.clk_pix += [
            If(ResetSignal("clk_pix"),
                H_Cont.eq(0),
                V_Cont.eq(0)
            ).Else(
                # H_Sync Counter
                If( H_Cont < h_sync_total,
                    H_Cont.eq(H_Cont + 1)
                ).Else(
                    H_Cont.eq(0)
                ),
                # H_Sync Generator
                If( (H_Cont > h_sync_front) & ( H_Cont < h_sync_front + h_sync_cyc),
                    self.hsync.eq(0)
                ).Else(
                    self.hsync.eq(1)
                ),

                If(H_Cont==0,
                    # V_Sync Counter
                    If(V_Cont < V_SYNC_TOTAL,
                        V_Cont.eq(V_Cont + 1)
                    ).Else(
                        V_Cont.eq(0)
                    ),
                    # V_Sync Generator
                    If((V_Cont > v_sync_front) & ( V_Cont < v_sync_front + v_sync_cyc),
                        self.vsync.eq(0)
                    ).Else(
                        self.vsync.eq(1)
                    )
                ),
                # Blank signal Generator (1 clock ahead)
                If((H_Cont >= x_start  ) & (H_Cont < h_sync_total ) & (V_Cont >= y_start) & (V_Cont < v_sync_total) ,
                    self.oCoord_X.eq(H_Cont - X_START),
                    self.oCoord_Y.eq(V_Cont - Y_START),
                    self.blank.eq(0)
                ).Else(
                    self.blank.eq(1),
                ),
                
                    # cadre
                If((H_Cont == x_start) | (H_Cont == h_sync_total ) | (V_Cont == v_sync_total ) | (V_Cont == y_start ),
                    self.red.eq(1023),
                    self.green.eq(1023),
                    self.blue.eq(1023)
                ).Else(
                    self.red.eq(self.red_in),
                    self.green.eq(self.green_in),
                    self.blue.eq(self.blue_in)
                )
            )
            ]

if __name__ == "__main__":   
    from migen.build.platforms import de2
    plat = de2.Platform() 
    clk50 = plat.request("clk50")
    pix_pll = PLL(20, "pix", 0, "NORMAL", 11, 4)   # PLL to get the required pixel frequency (usually a multiple of 27MHz)
    vga = VGA(pix_pll.clk_out,plat.request("key",0))
    vga_out = plat.request("vga_out")
    vga.comb += [ vga_out.g.eq(vga.green),
        vga_out.r.eq(vga.red),
        vga_out.b.eq(vga.blue),
        vga_out.hsync.eq(vga.hsync),
        vga_out.vsync.eq(vga.vsync),
        vga_out.blank.eq(~vga.blank),
        vga_out.sync.eq(~(vga.vsync & vga.hsync)),
        vga_out.clk.eq(vga.clk_in)
        ]
    vga.comb += pix_pll.clk_in.eq(clk50)    # 27MHz need by the PLL
    vga.submodules += pix_pll
    
    # Debug LEDs
    vga.comb += [ plat.request("user_led").eq(vga.clk_out),
            plat.request("user_led").eq(vga.hsync),
            plat.request("user_led").eq(vga.vsync)
            ]
    plat.build(vga, run=True, build_dir="vga", build_name="de2vga", )
