#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 15 16:38:30 2018

@author: fabien
"""
DEBUG=False
import serial
import time
ser = serial.Serial('/dev/ttyUSB0', 9600)
while True:
    msg=bytearray(input('Message: '), 'utf-8')
    ser.write([0x12])
    ser.write(msg)
    ser.write([0x13])

ser.flush()
ser.close()
