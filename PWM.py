#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 31 17:26:10 2018
              _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _ 
CLK :       _| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_|

pwm_freq :    <-----------------number of CLK--------------------------->
            _                           _________________________________
pwm_in :     |_________________________|<------nb of CLK high----------->|_____

Knowing your clock frequency, ex:
    50MHz/2500 = 20KHz

@author: fabien
"""
from migen import *

class PWM(Module):
    def __init__(self, PWM_freq=2500):
        self.pwm_in = Signal(max=PWM_freq)
        self.PWM = Signal()

        count = Signal(max=PWM_freq)
		
        duty_cycle = Signal(max=PWM_freq)
        
        ###
       
        self.sync += [
                If(count < PWM_freq,
                    count.eq(count + 1)
                ).Else(
                    count.eq(0)
                ),
                If(count < self.pwm_in,
                    self.PWM.eq(1)
                ).Else(
                    self.PWM.eq(0)
                )
                ]
        
        
if __name__ == "__main__":
    import sys
    if sys.argv[1] == "sim":
        print("Testbenching PWM module...")
        dut=PWM(PWM_freq=100)
        def testbench(dut):
            yield dut.pwm_in.eq(10)
            for cycle in range(200):
                yield
            yield dut.pwm_in.eq(50)    
            for cycle in range(200):
                yield
            yield dut.pwm_in.eq(100)    
            for cycle in range(200):
                yield
            yield dut.pwm_in.eq(130)    # pwm_in should not be > PWM_freq    
            for cycle in range(200):    # Add an assert somewhere?
                yield
                    
        run_simulation(dut, testbench(dut), vcd_name="PWM.vcd")