# This file was created by <fabien@acathla.tk>

from migen.build.generic_platform import *
from migen.build.altera import AlteraPlatform
from migen.build.altera.programmer import USBBlaster


_io = [
    ("clk50", 0, Pins("N2"), IOStandard("3.3-V LVTTL")),
    ("clk27", 0, Pins("D13"), IOStandard("3.3-V LVTTL")),

    ("user_led", 0, Pins("AE22"), IOStandard("3.3-V LVTTL")),
    ("user_led", 1, Pins("AF22"), IOStandard("3.3-V LVTTL")),
    ("user_led", 2, Pins("W19"), IOStandard("3.3-V LVTTL")),
    ("user_led", 3, Pins("V18"), IOStandard("3.3-V LVTTL")),
    ("user_led", 4, Pins("U18"), IOStandard("3.3-V LVTTL")),
    ("user_led", 5, Pins("U17"), IOStandard("3.3-V LVTTL")),
    ("user_led", 6, Pins("AA20"), IOStandard("3.3-V LVTTL")),
    ("user_led", 7, Pins("Y18"), IOStandard("3.3-V LVTTL")),

    ("user_redled", 0, Pins("AE23"), IOStandard("3.3-V LVTTL")),
    ("user_redled", 1, Pins("AF23"), IOStandard("3.3-V LVTTL")),
    ("user_redled", 2, Pins("AB21"), IOStandard("3.3-V LVTTL")),
    ("user_redled", 3, Pins("AC22"), IOStandard("3.3-V LVTTL")),
    ("user_redled", 4, Pins("AD22"), IOStandard("3.3-V LVTTL")),
    ("user_redled", 5, Pins("AD23"), IOStandard("3.3-V LVTTL")),
    ("user_redled", 6, Pins("AD21"), IOStandard("3.3-V LVTTL")),
    ("user_redled", 7, Pins("AC21"), IOStandard("3.3-V LVTTL")),
    ("user_redled", 8, Pins("AA14"), IOStandard("3.3-V LVTTL")),
    ("user_redled", 9, Pins("Y13"), IOStandard("3.3-V LVTTL")),
    ("user_redled", 10, Pins("AA13"), IOStandard("3.3-V LVTTL")),
    ("user_redled", 11, Pins("AC14"), IOStandard("3.3-V LVTTL")),
    ("user_redled", 12, Pins("AD15"), IOStandard("3.3-V LVTTL")),
    ("user_redled", 13, Pins("AE15"), IOStandard("3.3-V LVTTL")),
    ("user_redled", 14, Pins("AF13"), IOStandard("3.3-V LVTTL")),
    ("user_redled", 15, Pins("AE13"), IOStandard("3.3-V LVTTL")),
    ("user_redled", 16, Pins("AE12"), IOStandard("3.3-V LVTTL")),
    ("user_redled", 17, Pins("AD12"), IOStandard("3.3-V LVTTL")),
                     
    ("key", 0, Pins("G26"), IOStandard("3.3-V LVTTL")),
    ("key", 1, Pins("N23"), IOStandard("3.3-V LVTTL")),
    ("key", 2, Pins("P23"), IOStandard("3.3-V LVTTL")),
    ("key", 3, Pins("W26"), IOStandard("3.3-V LVTTL")),

    ("sw", 0, Pins("N25"), IOStandard("3.3-V LVTTL")),
    ("sw", 1, Pins("N26"), IOStandard("3.3-V LVTTL")),
    ("sw", 2, Pins("P25"), IOStandard("3.3-V LVTTL")),
    ("sw", 3, Pins("AE14"), IOStandard("3.3-V LVTTL")),
    ("sw", 4, Pins("AF14"), IOStandard("3.3-V LVTTL")),
    ("sw", 5, Pins("AD13"), IOStandard("3.3-V LVTTL")),
    ("sw", 6, Pins("AC13"), IOStandard("3.3-V LVTTL")),
    ("sw", 7, Pins("C13"), IOStandard("3.3-V LVTTL")),
    ("sw", 8, Pins("B13"), IOStandard("3.3-V LVTTL")),
    ("sw", 9, Pins("A13"), IOStandard("3.3-V LVTTL")),
    ("sw", 10, Pins("N1"), IOStandard("3.3-V LVTTL")),
    ("sw", 11, Pins("P1"), IOStandard("3.3-V LVTTL")),
    ("sw", 12, Pins("P2"), IOStandard("3.3-V LVTTL")),
    ("sw", 13, Pins("T7"), IOStandard("3.3-V LVTTL")),
    ("sw", 14, Pins("U3"), IOStandard("3.3-V LVTTL")),
    ("sw", 15, Pins("U4"), IOStandard("3.3-V LVTTL")),
    ("sw", 16, Pins("V1"), IOStandard("3.3-V LVTTL")),
    ("sw", 17, Pins("V2"), IOStandard("3.3-V LVTTL")),

    ("hex", 0,
        Subsignal("SevenSeg", Pins("AF10 AB12 AC12 AD11 AE11 V14 V13"), IOStandard("3.3-V LVTTL"))
    ),

    ("hex", 1,
        Subsignal("SevenSeg", Pins("V20 V21 W21 Y22 AA24 AA23 AB24"), IOStandard("3.3-V LVTTL"))
    ),

    ("hex", 2,
        Subsignal("SevenSeg", Pins("AB23 V22 AC25 AC26 AB26 AB25 Y24"), IOStandard("3.3-V LVTTL"))
    ),

    ("hex", 3,
        Subsignal("SevenSeg", Pins("Y23 AA25 AA26 Y26 Y25 U22 W24"), IOStandard("3.3-V LVTTL"))
    ),

    ("hex", 4,
        Subsignal("SevenSeg", Pins("U9 U1 U2 T4 R7 R6 T3"), IOStandard("3.3-V LVTTL"))
    ),

    ("hex", 5,
        Subsignal("SevenSeg", Pins("T2 P6 P7 T9 R5 R4 R3"), IOStandard("3.3-V LVTTL"))
    ),

    ("hex", 6,
        Subsignal("SevenSeg", Pins("R2 P4 P3 M2 M3 M5 M4"), IOStandard("3.3-V LVTTL"))
    ),

    ("hex", 7,
        Subsignal("SevenSeg", Pins("L3 L2 L9 L6 L7 P9 N9"), IOStandard("3.3-V LVTTL"))
    ),

    ("serial", 0,
        Subsignal("tx", Pins("B25"), IOStandard("3.3-V LVTTL")),
        Subsignal("rx", Pins("C25"), IOStandard("3.3-V LVTTL"))
    ),

    ("sdram_clock", 0, Pins("AA7"), IOStandard("3.3-V LVTTL")),
    ("sdram", 0,
        Subsignal("a", Pins("T6 V4 V3 W2 W1 U6 U7 U5 W4 W3 Y1 V5")),
        Subsignal("ba", Pins("AE2 AE3")),
        Subsignal("cs_n", Pins("AC3")),
        Subsignal("cke", Pins("AA6")),
        Subsignal("ras_n", Pins("AB4")),
        Subsignal("cas_n", Pins("AB3")),
        Subsignal("we_n", Pins("AD3")),
        Subsignal("dq", Pins("V6 AA2 AA1 Y3 Y4 R8 T8 V7 W6 AB2 AB1 AA4 AA3 AC2 AC1 AA5")),
        Subsignal("dm", Pins("AD2 Y5")),
        IOStandard("3.3-V LVTTL")
    ),

    ("sram", 0,
        Subsignal("adr", Pins("AE4 AF4 AC5 AC6 AD4 AD5 AE5 AF5 AD6 AD7 V10 V9",
                              "AC7 W8 W10 Y10 AB8 AC8")),
        Subsignal("dat", Pins("AD8 AE6 AF6 AA9 AA10 AB10 AA11 Y11 AE7 AF7 AE8",
                              "AF8 W11 W12 AC9 AC10")),
        Subsignal("oe_n", Pins("AD10")),
        Subsignal("we_n", Pins("AE10")),
        Subsignal("ce_n", Pins("AC11")),
        Subsignal("ub_n", Pins("AF9")),
        Subsignal("lb_n", Pins("AE9")),
        IOStandard("3.3-V LVTTL")
    ),

    ("i2c", 0,
        Subsignal("sclk", Pins("A6")),
        Subsignal("sdat", Pins("B6")),
        IOStandard("3.3-V LVTTL")
    ),

    ("vga_out", 0,
        Subsignal("clk", Pins("B8")),   # to the ADC: ADV7123
        Subsignal("r", Pins("C8 F10 G10 D9 C9 A8 H11 H12 F11 E10")),
        Subsignal("g", Pins("B9 A9 C10 D10 B10 A10 G11 D11 E12 D12")),
        Subsignal("b", Pins("J13 J14 F12 G12 J10 J11 C11 B11 C12 B12")),
        Subsignal("hsync", Pins("A7")),
        Subsignal("vsync", Pins("D8")),
        Subsignal("blank", Pins("D6")),
        Subsignal("sync", Pins("B7")),
        IOStandard("3.3-V LVTTL")
    ),

    ("lcd", 0,  # Crystalfontz CFAH1602B-TMC-JP
        Subsignal("data", Pins("J1 J2 H1 H2 J4 J3 H4 H3")),
        Subsignal("bl_on", Pins("K2")), # Backlight
        Subsignal("en", Pins("K3")), # Chip_enable
        Subsignal("on", Pins("L4")), # Power on the LCD module
        Subsignal("rs", Pins("K1")), # High = DATA / Low = Instruction
        Subsignal("rw", Pins("K4")), # High = Read / Low = Write 
        IOStandard("3.3-V LVTTL")
    ),

    ("TD", 0,   # TV Decoder, also generates the 27MHz clock
        Subsignal("reset", Pins("C4")),
        Subsignal("clk27", Pins("C16")),
        Subsignal("hs", Pins("D5")),
        Subsignal("vs", Pins("K9")),
        Subsignal("data", Pins("J9 E8 H8 H10 G9 F9 D7 C7")),
        IOStandard("3.3-V LVTTL")
    ),
    ("GPIO", 0, Pins("D25 J22 E26 E25 F24 F23 J21 J20 F25 F26 N18 P18 G23 G24 K22 G25 H23 H24 J23 J24 H25 H26 H19 K18 K19 K21 K23 K24 L21 L20 J25 J26 L23 L24 L25 L19"), IOStandard("3.3-V LVTTL")),
    ("GPIO", 1, Pins("K25 K26 M22 M23 M19 M20 N20 M21 M24 M25 N24 P24 R25 R24 R20 T22 T23 T24 T25 T18 T21 T20 U26 U25 U23 U24 R19 T19 U20 U21 V26 V25 V24 V23 W25 W23"))
]

_connectors = [
    ("0", "D25 J22 E26 E25 F24 F23 J21 J20 F25 F26 N18 P18 G23 G24 K22 G25 H23 H24 J23 J24 H25 H26 H19 K18 K19 K21 K23 K24 L21 L20 J25 J26 L23 L24 L25 L19"),
    ("1", "K25 K26 M22 M23 M19 M20 N20 M21 M24 M25 N24 P24 R25 R24 R20 T22 T23 T24 T25 T18 T21 T20 U26 U25 U23 U24 R19 T19 U20 U21 V26 V25 V24 V23 W25 W23"),
]


class Platform(AlteraPlatform):
    default_clk_name = "clk50"
    default_clk_period = 20

    def __init__(self):
        AlteraPlatform.__init__(self, "EP2C35F672C6", _io, _connectors)

    def create_programmer(self):
        return USBBlaster()
