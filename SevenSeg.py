from migen import *

class SevenSeg(Module):
    def __init__(self):
        self.value = Signal(4)
        self.segments = Signal(7)

        ###
        self.comb += [
            Case( self.value-1 , {  # value starts at 1 ?!
                0: self.segments.eq(0b1111001),
                1: self.segments.eq(0b0100100),
                2: self.segments.eq(0b0110000),
                3: self.segments.eq(0b0011001),
                4: self.segments.eq(0b0010010),
                5: self.segments.eq(0b0000010),
                6: self.segments.eq(0b1111000),
                7: self.segments.eq(0b0000000),
                8: self.segments.eq(0b0011000),
                9: self.segments.eq(0b0001000),
                10: self.segments.eq(0b0000011),
                11: self.segments.eq(0b1000110),
                12: self.segments.eq(0b0100001),
                13: self.segments.eq(0b0000110),
                14: self.segments.eq(0b0001110),
                15: self.segments.eq(0b1000000),
                "default": self.segments.eq(0b0000000),
            })]

