#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  7 16:14:19 2018

@author: fabien
"""
from migen import *

class PLL(Module):
    def __init__(self, period_in, name, phase_shift, operation_mode, mul, div):
        self.clk_in = Signal()
        self.clk_out = Signal()

        self.specials += Instance("ALTPLL",
                                  p_bandwidth_type = "AUTO",
                                  p_clk0_divide_by = div,
                                  p_clk0_duty_cycle = 50,
                                  p_clk0_multiply_by = mul,
                                  p_clk0_phase_shift = "{}".format(str(phase_shift)),
                                  p_compensate_clock = "CLK0",
                                  p_inclk0_input_frequency = int(period_in*1000),
                                  p_intended_device_family = "Cyclone II",
                                  p_lpm_hint = "CBX_MODULE_PREFIX={}_pll".format(name),
                                  p_lpm_type = "altpll",
                                  p_operation_mode = operation_mode,
                                  i_inclk=self.clk_in,
                                  o_clk=self.clk_out,
                                  i_areset=0,
                                  i_clkena=0x3f,
                                  i_clkswitch=0,
                                  i_configupdate=0,
                                  i_extclkena=0xf,
                                  i_fbin=1,
                                  i_pfdena=1,
                                  i_phasecounterselect=0xf,
                                  i_phasestep=1,
                                  i_phaseupdown=1,
                                  i_pllena=1,
                                  i_scanaclr=0,
                                  i_scanclk=0,
                                  i_scanclkena=1,
                                  i_scandata=0,
                                  i_scanread=0,
                                  i_scanwrite=0
                                  )
