#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 31 16:40:05 2018

@author: fabien
"""

from migen import *

class quadDec(Module):
    def __init__(self, bits=16):
        self.A = Signal()
        self.B = Signal()
        self.position = Signal(bits_sign=(bits,True))    #, reset=(int(2**bits/2)))
        
        old_a = Signal()
        old_b = Signal()
        cur_a = Signal()
        cur_b = Signal()
        up_down = Signal() # It's moving...
        self.direction = Signal() # ...in that direction
        
        ###
        
        self.comb += [
                up_down.eq( (cur_a ^ old_a) ^ (cur_b ^ old_b) ),
                self.direction.eq(cur_a ^ old_b)
                ]
        
        self.sync += [
                old_a.eq(cur_a),
                old_b.eq(cur_b),
                cur_a.eq(self.A),
                cur_b.eq(self.B),
                If(up_down,
                   If(self.direction,
                      self.position.eq(self.position + 1)
                   ).Else(
                      self.position.eq(self.position - 1)   
                      )
                   )
                ]
    
        
class quadEnc(Module):
    def __init__(self, bits=16):
        self.A = Signal()
        self.B = Signal()
        self.position = Signal(bits)
        
        ###
        
if __name__ == "__main__":
    import sys
    if sys.argv[1] == "sim":
        print("Testbenching Quadrature decoder module...")
        dut=quadDec(bits=8)
        def testbench(dut):
            for cycle in range(12):
                yield dut.A.eq(1)
                yield
                yield dut.B.eq(1)
                yield
                yield dut.A.eq(0)
                yield
                yield dut.B.eq(0)
                yield
            
            for cycle in range(12):
                yield dut.B.eq(1)
                yield
                yield dut.A.eq(1)
                yield
                yield dut.B.eq(0)
                yield
                yield dut.A.eq(0)
                yield
                
                # TODO: Test more !
                    
        run_simulation(dut, testbench(dut), vcd_name="quadDec.vcd")